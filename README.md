# pipeline-data

Configuration files for use with the various triggers in [cki-tools].

See the documentation for [onboarding a new kernel tree] for details.

## Onboarded trees

- [baseline trees](documentation/baseline.md)
- [ofa trees](documentation/ofa.md)

[cki-tools]: https://gitlab.com/cki-project/cki-tools/
[onboarding a new kernel tree]: https://cki-project.org/l/adding-kernel-tree
